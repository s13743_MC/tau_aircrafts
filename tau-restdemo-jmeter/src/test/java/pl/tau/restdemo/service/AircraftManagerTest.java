package pl.tau.restdemo.service;

// przyklad na podstawie przykladow J. Neumanna

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.tau.restdemo.domain.Aircraft;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AircraftManagerTest {

	AircraftManager aircraftManager;

	private final static String COMPANY_1 = "Boeing";
	private final static int YEAROFPRODUCTION_1 = 2016;

	@Before
	public void setup() throws SQLException {
		aircraftManager  = new AircraftManagerImpl();
	}

	@After
	public void cleanup() throws SQLException {
	}

	@Test
	public void checkConnection() {
		assertNotNull(aircraftManager.getConnection());
	}

	@Test
	public void checkAdding() throws SQLException {
		Aircraft aircraft = new Aircraft();
		aircraft.setCompany(COMPANY_1);
		aircraft.setYearOfProduction(YEAROFPRODUCTION_1);

		aircraftManager.deleteAll();
		assertEquals(1, aircraftManager.addAircraft(aircraft));

		List<Aircraft> aircrafts = aircraftManager.getAllAircrafts();
		Aircraft aircraftRetrieved = aircrafts.get(0);

		assertEquals(COMPANY_1, aircraftRetrieved.getCompany());
		assertEquals(YEAROFPRODUCTION_1, aircraftRetrieved.getYearOfProduction());
	}

}
