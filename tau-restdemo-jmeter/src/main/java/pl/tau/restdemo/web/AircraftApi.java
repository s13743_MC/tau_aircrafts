package pl.tau.restdemo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.tau.restdemo.domain.Aircraft;
import pl.tau.restdemo.service.AircraftManager;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Simple web api demo -- try implementning post method
 * 
 * Created by tp on 24.04.17.
 */
@RestController
public class AircraftApi {

    @Autowired
    AircraftManager aircraftManager;

    @RequestMapping("/")
    public String index() {
        return "This is non rest, just checking if everything works.";
    }

    @RequestMapping(value = "/aircraft/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Aircraft getAircraft(@PathVariable("id") Long id) throws SQLException {
        return aircraftManager.getAircraft(id);
    }

    @RequestMapping(value = "/aircrafts", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Aircraft> getAircrafts(@RequestParam(value = "filter", required = false) String f) throws SQLException {
        List<Aircraft> aircrafts = new LinkedList<Aircraft>();
        for (Aircraft a : aircraftManager.getAllAircrafts()) {
            if (f == null) {
                aircrafts.add(a);
            } else if (a.getCompany().contains(f)) {
                aircrafts.add(a);
            }
        }
        return aircrafts;
    }

    @RequestMapping(value = "/aircraft",
        method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public Aircraft addAircraft(@RequestBody Aircraft a) {
        if (aircraftManager.addAircraft(a) < 1) return null;
        return a;
    }

    @RequestMapping(value = "/aircraft/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Long deleteAircraft(@PathVariable("id") Long id) throws SQLException {
        return new Long(aircraftManager.deleteAircraft(aircraftManager.getAircraft(id)));
    }

    @RequestMapping(value = "/aircrafts", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteAllAircrafts() throws SQLException {
        aircraftManager.deleteAll();
    }

    @RequestMapping(value = "/aircraft",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Aircraft updateAircraft(@RequestBody Aircraft a) throws SQLException {
        if (aircraftManager.updateAircraft(a) < 1) return null;
        return a;
    }
}
