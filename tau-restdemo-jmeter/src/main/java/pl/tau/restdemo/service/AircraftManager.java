package pl.tau.restdemo.service;

// w oparciu o przyklad J Neumanna, przerobiony przez T Puzniakowskiego

import pl.tau.restdemo.domain.Aircraft;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface AircraftManager {
	public Connection getConnection();
	public void setConnection(Connection connection) throws SQLException;
	public int addAircraft(Aircraft aircraft);
	public int deleteAircraft(Aircraft aircraft);
	public int updateAircraft(Aircraft aircraft) throws SQLException;
	public Aircraft getAircraft(long id) throws SQLException;
	public String introduceSelf();
	public int deleteAll();
	public List<Aircraft> getAllAircrafts();
}
