package pl.edu.pjatk.tau.aircrafts.service;

import pl.edu.pjatk.tau.aircrafts.domain.Aircraft;
import pl.edu.pjatk.tau.aircrafts.interfaces.AircraftInterface;
import pl.edu.pjatk.tau.aircrafts.interfaces.TimeSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AircraftsService implements AircraftInterface, TimeSource {

    private List<Aircraft> db = new ArrayList<Aircraft>();
    private TimeSource timeSource;

    private boolean creationTimeEnabled = true;
    private boolean modificationTimeEnabled = true;
    private boolean readTimeEnabled = true;

    public void addAircraft(Aircraft aircraft) {
        for (Aircraft aircraftFromDb : db) {
            if (aircraftFromDb.getId() == aircraft.getId()) {
                throw new IllegalArgumentException();
            }
        }
        if (creationTimeEnabled) {
            aircraft.setCreationTime(getCurrentDate());
        }
        db.add(aircraft);
    }

    public Aircraft getAircraftById(int id) {
        for (Aircraft aircraftFromDb : db) {
            if (aircraftFromDb.getId() == id) {
                if (readTimeEnabled) {
                    aircraftFromDb.setReadTime(getCurrentDate());
                }
                return aircraftFromDb;
            }
        }

        throw new NoSuchFieldError();
    }

    public List<Aircraft> readAll() {

        if (readTimeEnabled) {
            for (Aircraft aircraftFromDb : db) {
                aircraftFromDb.setReadTime(getCurrentDate());
            }
        }

        return db;
    }

    public void updateAircraft(Aircraft aircraft) {

        Aircraft aircraftFromDb = getAircraftById(aircraft.getId());

        if (!aircraftFromDb.getCompany().equals(aircraft.getCompany())) {
            aircraftFromDb.setCompany(aircraft.getCompany());
        }
        if (!aircraftFromDb.getRegistration().equals(aircraft.getRegistration())) {
            aircraftFromDb.setRegistration(aircraft.getRegistration());
        }

        if (modificationTimeEnabled) {
            aircraftFromDb.setModificationTime(getCurrentDate());
        }
    }

    public void deleteAircraftById(int id) {

        Aircraft aircraftFromDb = getAircraftById(id);
        db.remove(aircraftFromDb);
    }

    public long getCurrentDate() {
        return this.timeSource.getCurrentDate();
    }

    public void setTimeSource(TimeSource timesource) {
        this.timeSource = timesource;
    }

    public Map<String, Long> getAllTimeStampsForAircraftById(int id) {

        Map<String, Long> allTimeStamps = new HashMap<String, Long>();

        for (Aircraft aircraftFromDb : db) {
            if (aircraftFromDb.getId() == id) {

                allTimeStamps.put("creationTime", aircraftFromDb.getCreationTime());
                allTimeStamps.put("modificationTime", aircraftFromDb.getModificationTime());
                allTimeStamps.put("readTime", aircraftFromDb.getReadTime());

                return allTimeStamps;
            }
        }

        throw new NoSuchFieldError();
    }

    public boolean setCreationTimeDisabled() {
        return this.creationTimeEnabled = false;
    }

    public boolean setCreationTimeEnabled() {
        return this.creationTimeEnabled = true;
    }

    public boolean setModificationTimeDisabled() {
        return this.modificationTimeEnabled = false;
    }

    public boolean setModificationTimeEnabled() {
        return this.modificationTimeEnabled = true;
    }

    public boolean setReadTimeDisabled() {
        return this.readTimeEnabled = false;
    }

    public boolean setReadTimeEnabled() {
        return this.readTimeEnabled = true;
    }

    public boolean isCreationTimeEnabled() {
        return creationTimeEnabled;
    }

    public boolean isModificationTimeEnabled() {
        return modificationTimeEnabled;
    }

    public boolean isReadTimeEnabled() {
        return readTimeEnabled;
    }

    public List<Aircraft> searchAircraftsByRegex(String regex) {
        List<Aircraft> aircraftsFoundByRegex = new ArrayList<Aircraft>();

        for (Aircraft aircraft : db) {
            if (aircraft.toString().matches(regex)) {
                aircraftsFoundByRegex.add(aircraft);
            }
        }

        return aircraftsFoundByRegex;

    }

    public void deleteAircraftByListOfAircrafts(List<Aircraft> aircrafts) {

        Aircraft aircraftFromDb;

        for (Aircraft aircraftToDelete : aircrafts) {
            aircraftFromDb = this.getAircraftById(aircraftToDelete.getId());

            if (aircraftFromDb.getCompany().equals(aircraftToDelete.getCompany()) &&
                    aircraftFromDb.getRegistration().equals(aircraftToDelete.getRegistration())) {
                this.deleteAircraftById(aircraftToDelete.getId());
            }
        }
    }
}
