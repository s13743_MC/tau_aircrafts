package pl.edu.pjatk.tau.aircrafts.interfaces;

import pl.edu.pjatk.tau.aircrafts.domain.Aircraft;

import java.util.List;

public interface AircraftInterface {

    void addAircraft(Aircraft aircraft);
    Aircraft getAircraftById(int id);
    List<Aircraft> readAll();
    void updateAircraft(Aircraft aircraft);
    void deleteAircraftById(int id);
    long getCurrentDate();
}
