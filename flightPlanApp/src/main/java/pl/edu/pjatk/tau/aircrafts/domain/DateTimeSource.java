package pl.edu.pjatk.tau.aircrafts.domain;

import pl.edu.pjatk.tau.aircrafts.interfaces.TimeSource;

import java.util.Date;

public class DateTimeSource implements TimeSource {
    public long getCurrentDate() {
        return new Date().getTime();
    }
}
