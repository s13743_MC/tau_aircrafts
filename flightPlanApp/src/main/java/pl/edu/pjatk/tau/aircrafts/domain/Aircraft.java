package pl.edu.pjatk.tau.aircrafts.domain;

public class Aircraft {

	private int id;
	private String company;
	private String registration;
	private long creationTime;
	private long modificationTime;
	private long readTime;
	
	public Aircraft() {}
	
	public Aircraft(int id, String company, String registration) {
		super();
		this.id = id;
		this.company = company;
		this.registration = registration;
	}
	
	public Aircraft(int id, String company, String registration,
			long creationTime, long modificationTime, long readTime) {
		super();
		this.id = id;
		this.company = company;
		this.registration = registration;
		this.creationTime = creationTime;
		this.modificationTime = modificationTime;
		this.readTime = readTime;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getRegistration() {
		return registration;
	}
	public void setRegistration(String registration) {
		this.registration = registration;
	}
	
	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public long getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(long modificationTime) {
		this.modificationTime = modificationTime;
	}

	public long getReadTime() {
		return readTime;
	}

	public void setReadTime(long readTime) {
		this.readTime = readTime;
	}

//	@Override
//	public String toString() {
//		return "Aircraft [id=" + id + ", company=" + company
//				+ ", registration=" + registration + ", creationTime="
//				+ creationTime + ", modificationTime=" + modificationTime
//				+ ", readTime=" + readTime + "]";
//	}

	@Override
	public String toString() {
		return "Aircraft [id=" + id + ", company=" + company
				+ ", registration=" + registration + ", creationTime="
				+ creationTime + ", modificationTime=" + modificationTime
				+ ", readTime=" + readTime + "]";
	}
}
