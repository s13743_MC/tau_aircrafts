package pl.edu.pjatk.tau.aircrafts.interfaces;

public interface TimeSource {
    long getCurrentDate();
}
