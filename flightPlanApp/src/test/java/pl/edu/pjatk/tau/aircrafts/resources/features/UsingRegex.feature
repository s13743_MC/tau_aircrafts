Feature: Using Regex
    As a user
    I want to searching aircrafts by regex
    So I can easily find requested aircraft

    Scenario Outline: Searching aircrafts by regex
        Given database is created
        And I add aircrafts to database
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |
            | 2  | "Boeing" | "Dreamliner" |
            | 3  | "Boeing" | "JumboJet"   |
            | 4  | "Boeing" | "737-900"    |
            | 5  | "Boeing" | "737-800"    |
            | 6  | "Boeing" | "777"        |
        When I search aircrafts with regex "<regex>"
        Then I should see <amountOfAircrafts> aircrafts

        Examples:
            | regex          | amountOfAircrafts |
            | (.*)737(.*)    | 2                 |
            | (.*)Airbus(.*) | 1                 |
            | (.*)Boeing(.*) | 5                 |
            | (.*)DAIKA(.*)  | 1                 |