package pl.edu.pjatk.tau.aircrafts.resources.stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pl.edu.pjatk.tau.aircrafts.domain.Aircraft;
import pl.edu.pjatk.tau.aircrafts.domain.DateTimeSource;
import pl.edu.pjatk.tau.aircrafts.interfaces.TimeSource;
import pl.edu.pjatk.tau.aircrafts.service.AircraftsService;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class Stepdefs {

    private AircraftsService db;
    private List<Aircraft> aircraftsFoundByRegex;

    @Given("^database is created$")
    public void databaseIsCreated() throws Exception {
        db = new AircraftsService();
        TimeSource timesource = new DateTimeSource();
        db.setTimeSource(timesource);
        assertNotNull(db);
    }

    @Given("^I add aircrafts? to database$")
    public void iAddAircraftsToDatabase(List<Aircraft> aircrafts) throws Exception {
        for (Aircraft aircraft : aircrafts) {
            db.addAircraft(aircraft);
            assertTrue(db.readAll().contains(aircraft));
        }

//        assertEquals(aircrafts.size(), db.readAll().size());
    }

    @When("^I search aircrafts with regex \"([^\"]*)\"$")
    public void iSearchAircraftsWithRegex(String regex) throws Exception {
        aircraftsFoundByRegex = db.searchAircraftsByRegex(regex);
    }

    @Then("^I should see (\\d+) aircrafts$")
    public void iShouldSeeAircrafts(int aircraftsNumberExpected) throws Exception {
        assertEquals(aircraftsNumberExpected, aircraftsFoundByRegex.size());
    }

    @When("^I delete following aircrafts$")
    public void iDeleteFollowingAircrafts(List<Aircraft> aircrafts) throws Exception {
        db.deleteAircraftByListOfAircrafts(aircrafts);
    }

    @Then("^database contains following aircrafts?$")
    public void databaseContainsFollowingAircrafts(List<Aircraft> aircrafts) throws Exception {
        int aircraftsCounter = 0;

        for (Aircraft aircraftExpected : aircrafts) {
            for (Aircraft aircraftFromDb : db.readAll()) {
                if (aircraftFromDb.getId() == (aircraftExpected.getId()) &&
                        aircraftFromDb.getCompany().equals(aircraftExpected.getCompany()) &&
                        aircraftFromDb.getRegistration().equals(aircraftExpected.getRegistration())) {
                    aircraftsCounter++;
                }
            }
        }

        assertEquals(aircraftsCounter, db.readAll().size());
    }

    @When("^I disable setting \"([^\"]*)\" time$")
    public void iDisableSettingTime(String timeStampType) throws Exception {
        if (timeStampType.equalsIgnoreCase("creation")) {
            db.setCreationTimeDisabled();
            assertEquals(false, db.isCreationTimeEnabled());
        } else if (timeStampType.equalsIgnoreCase("read")) {
            db.setReadTimeDisabled();
            assertEquals(false, db.isReadTimeEnabled());
        } else if (timeStampType.equalsIgnoreCase("modification")) {
            db.setModificationTimeDisabled();
            assertEquals(false, db.isModificationTimeEnabled());
        } else {
            throw new NoSuchElementException();
        }
    }

    @Then("^\"([^\"]*)\" time for aircraft with id (\\d+) is (\\d+)$")
    public void timeForAircraftWithIdIs(String timeStampType, int id, int time) throws Exception {
        if (timeStampType.equalsIgnoreCase("creation")) {
            assertEquals(time, db.getAircraftById(id).getCreationTime());
        } else if (timeStampType.equalsIgnoreCase("read")) {
            assertEquals(time, db.getAircraftById(id).getReadTime());
        } else if (timeStampType.equalsIgnoreCase("modification")) {
            assertEquals(time, db.getAircraftById(id).getModificationTime());
        } else {
            throw new NoSuchElementException();
        }
    }

    @Then("^I enable setting \"([^\"]*)\" time$")
    public void iEnableSettingTime(String timeStampType) throws Exception {
        if (timeStampType.equalsIgnoreCase("creation")) {
            db.setCreationTimeEnabled();
            assertEquals(true, db.isCreationTimeEnabled());
        } else if (timeStampType.equalsIgnoreCase("read")) {
            db.setReadTimeEnabled();
            assertEquals(true, db.isReadTimeEnabled());
        } else if (timeStampType.equalsIgnoreCase("modification")) {
            db.setModificationTimeEnabled();
            assertEquals(true, db.isModificationTimeEnabled());
        } else {
            throw new NoSuchElementException();
        }
    }

    @Then("^\"([^\"]*)\" time for aircraft with id (\\d+) is set$")
    public void timeForAircraftWithIdIsSet(String timeStampType, int id) throws Exception {
        if (timeStampType.equalsIgnoreCase("creation")) {
            assertTrue(db.getAircraftById(id).getCreationTime() > 0);
        } else if (timeStampType.equalsIgnoreCase("read")) {
            assertTrue(db.getAircraftById(id).getReadTime() > 0);
        } else if (timeStampType.equalsIgnoreCase("modification")) {
            assertTrue(db.getAircraftById(id).getCreationTime() > 0);
        } else {
            throw new NoSuchElementException();
        }
    }
}