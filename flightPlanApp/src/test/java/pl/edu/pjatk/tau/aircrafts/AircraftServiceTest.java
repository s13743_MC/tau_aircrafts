package pl.edu.pjatk.tau.aircrafts;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.edu.pjatk.tau.aircrafts.domain.Aircraft;
import pl.edu.pjatk.tau.aircrafts.interfaces.TimeSource;
import pl.edu.pjatk.tau.aircrafts.service.AircraftsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

//import static org.assertj.core.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class AircraftServiceTest {
	
	AircraftsService db;
	long mockedTime;

	@Mock
	TimeSource timeSource;
	
	@Before
	public void setMockForDate() {
		
		mockedTime = 123456;
		
		when(timeSource.getCurrentDate()).thenReturn(mockedTime);
		
		db = new AircraftsService();
		
		db.setTimeSource(timeSource);
	}
	
	@Test
	public void createAircraftObject() {
		Aircraft aircraft = new Aircraft();
		assertNotNull(aircraft);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void createAircraftTest() {
		Aircraft aircraft1 = new Aircraft(1, "Airbus", "DAIKA");
		Aircraft aircraft2 = new Aircraft(1, "Airbus", "DAIKA");
		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
	}
	
	@Test(expected = NoSuchFieldError.class)
	public void getAircraftFromDb() {
		int id = 1;
		db.getAircraftById(id);
	}
	
	@Test
	public void checkIfAircraftIsAdded() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "JumboJet");
		db.addAircraft(aircraft1);
		
		assertEquals(aircraft1, db.getAircraftById(1));
	}
	
	@Test
	public void checkReadAllAircrafts() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "JumboJet");
		Aircraft aircraft2 = new Aircraft(2, "Airbus", "DAIKA");
		
		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
		
		List<Aircraft> newListWithAircrafts = new ArrayList<Aircraft>();
		newListWithAircrafts.add(aircraft1);
		newListWithAircrafts.add(aircraft2);
		
		assertEquals(newListWithAircrafts, db.readAll());
	}
	
	@Test
	public void checkReadOneAircraft() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "JumboJet");
		
		db.addAircraft(aircraft1);
		
		assertEquals(aircraft1, db.getAircraftById(1));
	}
	
	@Test
	public void checkUpdateCompanyAircraft() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "JumboJet");
		Aircraft aircraft2 = new Aircraft(1, "Airbus", "JumboJet");
		
		db.addAircraft(aircraft1);
		
		db.updateAircraft(aircraft2);
		
		assertEquals(aircraft2.getCompany(), db.getAircraftById(1).getCompany());
	}
	
	@Test
	public void checkUpdateRegistrationAicraft() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(2, "Boeing", "747");
		Aircraft aircraft3 = new Aircraft(2, "Boeing", "JumboJet");
		
		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
		
		db.updateAircraft(aircraft3);
		
		assertEquals(aircraft3.getRegistration(), db.getAircraftById(2).getRegistration());
	}
	
	@Test(expected = NoSuchFieldError.class)
	public void checkUpdateIdNotExist() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(2, "Boeing", "747");
		Aircraft aircraft3 = new Aircraft(3, "Boeing", "JumboJet");
		
		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
		
		db.updateAircraft(aircraft3);
	}
	
	@Test
	public void checkDeleteAircraftFromDb() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		
		db.addAircraft(aircraft1);
		
		db.deleteAircraftById(1);
		
		assertEquals(db.readAll().size(), 0);
	}
	
	@Test
	public void checkDeleteAircraftFromDbWithMoreData() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(2, "Boeing", "747");
		Aircraft aircraft3 = new Aircraft(3, "Boeing", "JumboJet");
		
		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
		db.addAircraft(aircraft3);
		
		db.deleteAircraftById(1);
		db.deleteAircraftById(3);
		
		assertEquals(db.readAll().size(), 1);
	}

	@Test
	public void checkTimeStampAfterReadOneRecord() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");

		db.addAircraft(aircraft1);

		assertEquals(mockedTime, db.getAircraftById(1).getReadTime());
	}

	@Test
	public void checkTimeStampAfterReadAllRecords() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(2, "Boeing", "747");
		Aircraft aircraft3 = new Aircraft(3, "Boeing", "JumboJet");

		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
		db.addAircraft(aircraft3);

		assertEquals(mockedTime, db.readAll().get(0).getReadTime());
		assertEquals(mockedTime, db.readAll().get(1).getReadTime());
		assertEquals(mockedTime, db.readAll().get(2).getReadTime());
	}

	@Test
	public void checkTimeStampAfterAddNewAircraft() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");

		db.addAircraft(aircraft1);

		assertEquals(mockedTime, db.getAircraftById(1).getCreationTime());
	}

	@Test
	public void checkTimeStampAfterUpdateAircraft() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "JumboJet");
		Aircraft aircraft2 = new Aircraft(1, "Airbus", "JumboJet");

		db.addAircraft(aircraft1);

		db.updateAircraft(aircraft2);

		assertEquals(mockedTime, db.getAircraftById(1).getModificationTime());
	}

	@Test
	public void checkGetAllTimeStampsForAircraftById() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(1, "Airbus", "747");

		db.addAircraft(aircraft1);
		db.getAircraftById(1);
		db.updateAircraft(aircraft2);

		Map<String, Long> timeStamps = new HashMap<String, Long>();
		timeStamps.put("creationTime", mockedTime);
		timeStamps.put("modificationTime", mockedTime);
		timeStamps.put("readTime", mockedTime);

		Map<String, Long> timeStampsOfAircraft = db.getAllTimeStampsForAircraftById(1);

		assertEquals(timeStamps, timeStampsOfAircraft);
	}

	@Test(expected = NoSuchFieldError.class)
	public void checkGetAllTimeStampsForAircraftByIdNotExists() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");

		db.addAircraft(aircraft1);

		db.getAllTimeStampsForAircraftById(2);
	}

	@Test
	public void checkSetCreationTimeDisabled() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");

		db.setCreationTimeDisabled();

		db.addAircraft(aircraft1);

		assertEquals(0, db.getAircraftById(1).getCreationTime());
	}

	@Test
	public void checkSetCreationTimeEnabled() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(2, "Airbus", "A330");

		db.setCreationTimeDisabled();

		db.addAircraft(aircraft1);

		db.setCreationTimeEnabled();

		db.addAircraft(aircraft2);

		assertEquals(0, db.getAircraftById(1).getCreationTime());
		assertEquals(mockedTime, db.getAircraftById(2).getCreationTime());
	}

	@Test
	public void checkSetModificationTimeDisabled() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "JumboJet");
		Aircraft aircraft2 = new Aircraft(1, "Airbus", "JumboJet");

		db.addAircraft(aircraft1);

		db.setModificationTimeDisabled();

		db.updateAircraft(aircraft2);

		assertEquals(0, db.getAircraftById(1).getModificationTime());
	}

	@Test
	public void checkSetModificationTimeEnabled() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "JumboJet");
		Aircraft aircraft2 = new Aircraft(1, "Airbus", "JumboJet");

		db.addAircraft(aircraft1);

		db.setModificationTimeDisabled();

		db.updateAircraft(aircraft2);

		assertEquals(0, db.getAircraftById(1).getModificationTime());

		db.setModificationTimeEnabled();

		db.updateAircraft(aircraft2);

		assertEquals(mockedTime, db.getAircraftById(1).getModificationTime());
	}

	@Test
	public void checkSetReadTimeDisabled() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");

		db.addAircraft(aircraft1);

		db.setReadTimeDisabled();

		db.getAircraftById(1);

		assertEquals(0, db.getAircraftById(1).getReadTime());
	}

	@Test
	public void checkSetReadTimeEnabled() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");

		db.addAircraft(aircraft1);

		db.setReadTimeDisabled();

		db.getAircraftById(1);

		assertEquals(0, db.getAircraftById(1).getReadTime());

		db.setReadTimeEnabled();

		db.getAircraftById(1);

		assertEquals(mockedTime, db.getAircraftById(1).getReadTime());
	}

	@Test
	public void checkSetReadTimeDisabledAfterReadAllRecords() {

		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(2, "Boeing", "747");
		Aircraft aircraft3 = new Aircraft(3, "Boeing", "JumboJet");

		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
		db.addAircraft(aircraft3);

		db.setReadTimeDisabled();

		assertEquals(0, db.readAll().get(0).getReadTime());
		assertEquals(0, db.readAll().get(1).getReadTime());
		assertEquals(0, db.readAll().get(2).getReadTime());
	}

	@Test
	public void checksearchAircraftsByRegex() {
		Aircraft aircraft1 = new Aircraft(1, "Boeing", "787");
		Aircraft aircraft2 = new Aircraft(2, "Airbus", "A330");
		Aircraft aircraft3 = new Aircraft(3, "Boeing", "JumboJet");

		db.addAircraft(aircraft1);
		db.addAircraft(aircraft2);
		db.addAircraft(aircraft3);

		List<Aircraft> aircraftsFoundByRegex = db.searchAircraftsByRegex("(.*)Boeing(.*)");

		List<Aircraft> aircraftsFoundByRegexExpected = new ArrayList<Aircraft>();
		aircraftsFoundByRegexExpected.add(aircraft1);
		aircraftsFoundByRegexExpected.add(aircraft3);

		assertEquals(aircraftsFoundByRegexExpected, aircraftsFoundByRegex);
	}
}