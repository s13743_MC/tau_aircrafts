Feature: Aircraft Service test
    As a user
    I want to use methods from Aircraft Service
    So I can cen be sure that they works correctly

    Scenario: Deleting aircrafts with a list of aircrafts
        Given database is created
        And I add aircrafts to database
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |
            | 2  | "Boeing" | "Dreamliner" |
            | 3  | "Boeing" | "JumboJet"   |
            | 4  | "Boeing" | "737-900"    |
            | 5  | "Boeing" | "737-800"    |
            | 6  | "Boeing" | "777"        |
        When I delete following aircrafts
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |
            | 4  | "Boeing" | "737-900"    |
        Then database contains following aircrafts
            | id | company  | registration |
            | 2  | "Boeing" | "Dreamliner" |
            | 3  | "Boeing" | "JumboJet"   |
            | 5  | "Boeing" | "737-800"    |
            | 6  | "Boeing" | "777"        |

    Scenario: Aircraft can be added to database
        Given database is created
        When I add aircraft to database
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |
        Then database contains following aircraft
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |

    Scenario: Checking disabling of creation time setting
        Given database is created
        When I disable setting "creation" time
        And I add aircraft to database
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |
        Then "Creation" time for aircraft with id 1 is 0
        But I enable setting "creation" time
        And I add aircraft to database
            | id | company  | registration |
            | 2  | "Boeing" | "DAIKA"      |
        Then "Creation" time for aircraft with id 2 is set

    Scenario: Checking disabling of read time setting
        Given database is created
        When I disable setting "read" time
        And I add aircraft to database
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |
        Then "Read" time for aircraft with id 1 is 0
        But I enable setting "read" time
        And I add aircraft to database
            | id | company  | registration |
            | 2  | "Boeing" | "DAIKA"      |
        Then "Read" time for aircraft with id 2 is set

    Scenario: Checking disabling of modification time setting
        Given database is created
        When I disable setting "modification" time
        And I add aircraft to database
            | id | company  | registration |
            | 1  | "Airbus" | "DAIKA"      |
        Then "Modification" time for aircraft with id 1 is 0
        But I enable setting "modification" time
        And I add aircraft to database
            | id | company  | registration |
            | 2  | "Boeing" | "DAIKA"      |
        Then "Modification" time for aircraft with id 2 is set